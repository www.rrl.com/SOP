package com.gitee.sop.adminserver.api.isv.result;

import lombok.Data;

@Data
public class AppKeySecretVo {
    private String appKey;
    private String secret;
}
