# 原理分析之路由存储

SOP将路由信息存到了zookeeper当中，服务在启动时，将自己的路由信息上传到zookeeper中。
网关监听存放路由的节点，动态更新到本地。

zookeeper存储路由的结构如下：

```xml
/com.gitee.sop.route-<profile>   根节点
      /serviceId        服务节点，名字为服务名
           /route1      路由节点，名字为：name+version，存放路由信息
           /route2
           /...
```

服务启动时，创建`/serviceId`节点，然后遍历创建`/routeN`节点

同时，网关监听`服务节点`和`路由节点`，当有新服务加入时，网关会获取到新加入的路由节点信息，
同时路由节点下面的子节点也会被监听到。后续子节点的增删改都会被网关监听到，然后更新到本地。

服务上传路由相关代码在`com.gitee.sop.servercommon.manager.ServiceZookeeperApiMetaManager`类中

网关监听相关代码在`com.gitee.sop.gatewaycommon.manager.BaseRouteManager`中


